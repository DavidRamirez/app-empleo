const express = require('express')
const router = express.Router();
const userController = require('../../controllers/users/userController');


// iniciar sesion
router.get('/iniciar-sesion', userController.formLogin)
router.post('/iniciar-sesion', userController.login)

// crear usuario
router.get('/crear-cuenta', userController.formCreateUser)
router.post('/crear-cuenta', userController.createUser)




module.exports = router