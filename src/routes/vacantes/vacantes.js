const express = require('express')
const router = express.Router();
const vacantesControler = require('../../controllers/vacantes/vacantesController')

router.get('/vacantes', vacantesControler.mostrarVacantes)



module.exports = router;