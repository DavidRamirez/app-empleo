const express = require('express');
const path = require('path')
const morgan = require('morgan');
const pug = require('pug');
const bodyParser = require('body-parser')

// Initializations
const app = express();

// Settings
const port = process.env.PORT || 3000;
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


// Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))


// Global Variables


// Routes
app.use(require('./routes/index'))  
app.use(require('./routes/users/user'))  
app.use(require('./routes/reclutadores/reclutadores'))  
app.use(require('./routes/vacantes/vacantes'))  



// Static Files
app.use(express.static(path.join(__dirname, 'public')));


// Starting the server
app.listen(port, () => console.log(`Example app listening on port port!`))

